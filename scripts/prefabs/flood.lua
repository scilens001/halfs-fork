local assets = {
	Asset("ANIM", "anim/floodtile.zip"),
}

--136 is the texture dimension
--4.4813 is the scale applied in spriter.

local s = .7025--math.sqrt(1 * 300 / (136 * 4.4813)) + 0.00075 --see https://forums.kleientertainment.com/forums/topic/99705-info-scaling-ground-textures-to-in-game-units/ -Z

--for networking entities sans actions (a lot less costly)
--For some reason, the basegame entityscript overrides this function.
local AddNetworkProxy = UpvalueHacker.GetUpvalue(Entity.AddNetwork, "AddNetworkProxy")
if not AddNetworkProxy then
    AddNetworkProxy = Entity.AddNetwork
    print("WARNING: IA could not find AddNetworkProxy, tides and flood are going to be very laggy!")
end

local function network_fn()
    local inst = CreateEntity()

    AddNetworkProxy(inst.entity)
    inst.entity:AddTransform()

    inst:AddTag("CLASSIFIED")

    inst.persists = false

    if not TheNet:IsDedicated() then
        inst:DoTaskInTime(1, function(inst)
            if not TheWorld.ismastersim then
                TheWorld.components.flooding:AddFloodTile(inst)
            end
            inst.floodx, inst.floody = TheWorld.components.flooding:RealToFloodPos(inst.Transform:GetWorldPosition())
            SetTileState(inst.floodx, inst.floody, "flood", true)
        end)

        function inst:OnRemoveEntity()
            if not TheWorld.ismastersim then
                TheWorld.components.flooding:RemoveFloodTile(inst)
            end
            SetTileState(inst.floodx, inst.floody, "flood", nil)
        end
    end

    return inst
end

local function visual_fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()

    inst.entity:AddAnimState()
    inst.AnimState:SetRayTestOnBB(true)
    inst.Transform:SetScale(s,s,s)

    inst.persists = false

    return inst
end

return Prefab("network_flood", network_fn),
    Prefab("visual_flood", visual_fn, assets)