local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Drownable = require("components/drownable")

local _ShouldDrown = Drownable.ShouldDrown
function Drownable:ShouldDrown(...)
	return _ShouldDrown(self, ...) and not (self.inst.components.sailor and self.inst.components.sailor:IsSailing())
end

--just a patch for the weregoose in the long run better support for dst and ia drowning should be added....
local _IsOverWater = Drownable.IsOverWater
function Drownable:IsOverWater()
    if self.inst:HasTag("weregoose") and IsOnWater(self.inst) then
		return true
	else
		return _IsOverWater(self)
	end
end
