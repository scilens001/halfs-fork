local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddComponentPostInit("dynamicmusic", function(self, inst)
    if IA_CONFIG.dynamicmusic == false then
		return
	end

    local OnEnableDynamicMusic = inst:GetEventCallbacks("enabledynamicmusic", TheWorld)
	local OnPlayerActivated = inst:GetEventCallbacks("playeractivated", inst, "scripts/components/dynamicmusic.lua")

	if not (OnEnableDynamicMusic and OnPlayerActivated) then
		return
	end

	local StartPlayerListeners = UpvalueHacker.GetUpvalue(OnPlayerActivated, "StartPlayerListeners")

	if not StartPlayerListeners then
		return
	end

	local StopBusy = UpvalueHacker.GetUpvalue(OnEnableDynamicMusic, "StopBusy")
    local StartBusy = UpvalueHacker.GetUpvalue(StartPlayerListeners, "StartBusy")
	local StopDanger = UpvalueHacker.GetUpvalue(OnEnableDynamicMusic, "StopDanger")
    local OnAttacked = UpvalueHacker.GetUpvalue(StartPlayerListeners, "OnAttacked")
	local StartTriggeredDanger = UpvalueHacker.GetUpvalue(StartPlayerListeners, "StartTriggeredDanger")

	if not (StopBusy and StartBusy and StopDanger and OnAttacked) then
		return
	end

	local StartDanger = UpvalueHacker.GetUpvalue(OnAttacked, "StartDanger")

	local get_dangertask = function() return UpvalueHacker.GetUpvalue(StartDanger, "_dangertask") end
	local get_extendtime = function() return UpvalueHacker.GetUpvalue(StopBusy, "_extendtime") end
	local get_triggeredlevel = function() return UpvalueHacker.GetUpvalue(StartTriggeredDanger, "_triggeredlevel") end
    local set_busytheme = function(theme) UpvalueHacker.SetUpvalue(StartBusy, theme, "_busytheme") end

    local _isenabled = nil
	local _soundemitter = nil

    local _isday = nil
    local _iscave = inst:HasTag("cave")
	local _startsailmusictick = 0
	local _stopsailmusictick = 0
    local _sailing = false
	local _sailtask = nil
    local _iserupting = false
    local _playsIAmusic = false
	local _activatedplayer =  nil

    local soundAlias = {
    	--busy
    	["dontstarve/music/music_work"] = "ia/music/music_work_season_1",
    	["dontstarve/music/music_work_winter"] = "ia/music/music_work_season_2",
    	["dontstarve_DLC001/music/music_work_spring"] = "ia/music/music_work_season_3",
    	["dontstarve_DLC001/music/music_work_summer"] = "ia/music/music_work_season_4",
    	--combat
    	["dontstarve/music/music_danger"] = "ia/music/music_danger_season_1",
    	["dontstarve/music/music_danger_winter"] = "ia/music/music_danger_season_2",
    	["dontstarve_DLC001/music/music_danger_spring"] = "ia/music/music_danger_season_3",
    	["dontstarve_DLC001/music/music_danger_summer"] = "ia/music/music_danger_season_4",
    	--epic
    	["dontstarve/music/music_epicfight"] = "ia/music/music_epicfight_season_1",
    	["dontstarve/music/music_epicfight_winter"] = "ia/music/music_epicfight_season_2",
    	["dontstarve_DLC001/music/music_epicfight_spring"] = "ia/music/music_epicfight_season_3",
    	["dontstarve_DLC001/music/music_epicfight_summer"] = "ia/music/music_epicfight_season_4",
    	--stinger
    	["dontstarve/music/music_dawn_stinger"] = "ia/music/music_dawn_stinger",
    	["dontstarve/music/music_dusk_stinger"] = "ia/music/music_dusk_stinger",
    }

------------------------------Adding IA Climate Music---------------------------------
	local function IA_MusicSwap(_inst, climate)
		if IsIAClimate(climate) then
			if not _playsIAmusic then
    			-- print("CHANGE TO IA MUSIC")
    			for k, v in pairs(soundAlias) do
    				SetSoundAlias(k, v)
    			end

    			_playsIAmusic = true
    			set_busytheme(nil)
    		end
		elseif _playsIAmusic then
			-- print("CHANGE TO DST MUSIC")
    		for k, v in pairs(soundAlias) do
    			SetSoundAlias(k, nil)
    		end

    		_playsIAmusic = false
    		set_busytheme(nil)
		end
	end
---------------------------------------------------------------------------------------


----------------------------------Adding Sailing Music---------------------------------
	local function StartSailing(player, music_type)
		if player and IsInIAClimate(player) and _soundemitter then
			_soundemitter:PlaySound("ia/music/music_" .. music_type, music_type)
			_soundemitter:SetParameter(music_type, "intensity", 0)
		end
	end

	local function StopSailing(sail_type)
		if _soundemitter then
			if sail_type then
				_soundemitter:SetParameter(sail_type .. "_day", "intensity", 0)
				_soundemitter:SetParameter(sail_type .. "_night", "intensity", 0)
			else
				_soundemitter:SetParameter("sailing_day", "intensity", 0)
				_soundemitter:SetParameter("sailing_night", "intensity", 0)
				_soundemitter:SetParameter("surfing_day", "intensity", 0)
				_soundemitter:SetParameter("surfing_night", "intensity", 0)
			end

			_sailing = false
		end
	end

	local function OnStartSailing(player, sail_type)
		if not _isenabled or not player or not IsInIAClimate(player) or not _soundemitter then
			return
		end

		local music_type = sail_type .. (_isday and "_day" or "_night")
		StartSailing(player, music_type)
		StopSailing(sail_type)

		if not _soundemitter:PlayingSound("dawn") then
			if not _sailing then
				_sailing = true
				local _dangertask = get_dangertask()
				local _extendtime = get_extendtime() or 0
				if _extendtime <= GetTime() and not _dangertask and not _soundemitter:PlayingSound("erupt") then
					StopBusy(inst)
					_soundemitter:SetParameter(music_type, "intensity", 1)
				end
			end
		end
	end

	local function OnPhase(_inst, phase)
		_isday = phase == "day"

		if _isday or phase == "dusk" then
			StopSailing()
		end
	end

	local function OnEmbark(player, data)
		local boat = data.target
		if not boat then
			return
		end

		_sailtask = player:DoPeriodicTask(1, function()
			local sail_type = boat:HasTag("surfboard") and "surfing" or "sailing"
			if player:HasTag("moving") and player:HasTag("sailing") then
				_stopsailmusictick = 0

				if _startsailmusictick ~= 3 then
					_startsailmusictick = _startsailmusictick + 1
					return
				end

				OnStartSailing(player, sail_type)
			else
				_startsailmusictick = 0

				if _stopsailmusictick ~= 3 then
					_stopsailmusictick = _stopsailmusictick + 1
					return
				end
				StopSailing()
			end
		end)
	end

	local function OnDisembark()
		if _sailtask then
			_sailtask:Cancel()
			_sailtask = nil
		end

		StopSailing()
	end

---------------------------------------------------------------------------------------


--------------------------------Adding Volcano Music-----------------------------------
	local function StartErupt(player)
		if not _isenabled or not player or not IsInIAClimate(player) or not _soundemitter then
			return
		end

		if not _soundemitter:PlayingSound("erupt") then
			StopBusy()
			StopDanger()
			-- StopPlayingTone() Hamlet OST
			StopSailing()

			_soundemitter:PlaySound("ia/music/music_volcano_active", "erupt")
			_soundemitter:KillSound("dawn")
		end

		_iserupting = true
	end

	local function StopErupt()
		if _soundemitter then
			_soundemitter:KillSound("erupt")
			_iserupting = false
		end
	end

	local function OnPlayerArrive(_inst, player)
		if player and player == _activatedplayer and IsInClimate(player, "volcano") and _soundemitter then
			if _iserupting or player.player_classified.smokerate:value() > 0 then
				_soundemitter:PlaySound("ia/music/music_volcano_active")
			else
				_soundemitter:PlaySound("ia/music/music_volcano_dormant")
			end
		end
	end
---------------------------------------------------------------------------------------


-----------------------------------Hooking StartDanger---------------------------------
	local function IA_StartDanger(...)
		local _dangertask = get_dangertask()
		if _dangertask == nil and _isenabled then
            StopSailing()
		end

		StartDanger(...)
	end
	UpvalueHacker.SetUpvalue(OnAttacked, IA_StartDanger, "StartDanger")
	gemrun("hidefn", IA_StartDanger, StartDanger)
---------------------------------------------------------------------------------------


------------------------------------Hooking StartBusy----------------------------------
	local function IA_StartBusy(player, ...)
		if not _sailing then
			-- Theres a bug where the "busy" sound is killed without clearing the busytheme..
			-- I cant figure out why so heres a hacky fix.....
			if not _soundemitter:PlayingSound("busy") then
				set_busytheme(nil)
			end
			StartBusy(player, ...)
		end
	end
	UpvalueHacker.SetUpvalue(StartPlayerListeners, IA_StartBusy, "StartBusy")
	gemrun("hidefn", IA_StartBusy, StartBusy)
---------------------------------------------------------------------------------------


-------------------------------Hooking StartTriggeredDanger----------------------------
	local function IA_StartTriggeredDanger(player, data, ...)
		local level = math.max(1, math.floor(data ~= nil and data.level or 1))
		local _triggeredlevel = get_triggeredlevel()
		if _triggeredlevel ~= level and _isenabled then
			StopSailing()
		end

		StartTriggeredDanger(player, data, ...)
	end

	UpvalueHacker.SetUpvalue(StartPlayerListeners, IA_StartTriggeredDanger, "StartTriggeredDanger")
	gemrun("hidefn", IA_StartTriggeredDanger, StartTriggeredDanger)
---------------------------------------------------------------------------------------


	local function IA_StartPlayerListeners(player)
		IA_MusicSwap(player, GetClimate(player))  -- on load
		inst:ListenForEvent("climatechange", IA_MusicSwap, player)
		player:DoTaskInTime(0, function() IA_MusicSwap(player, GetClimate(player)) end)

		if player.replica and player.replica.sailor then  -- on load
			OnEmbark(player, {target = player.replica.sailor:GetBoat()})
		end
		inst:ListenForEvent("embarkboat", OnEmbark, player)
		inst:ListenForEvent("disembarkboat", OnDisembark, player)

		inst:ListenForEvent("playerentered", OnPlayerArrive)
		inst:ListenForEvent("OnVolcanoEruptionBegin", StartErupt, player)
		inst:ListenForEvent("OnVolcanoEruptionEnd", StopErupt, player)

		inst:ListenForEvent("QuackenEncounter", IA_StartDanger, player)  --Danger music on Quacken spawn
	end

	local function IA_StopPlayerListeners(player)
		inst:RemoveEventCallback("climatechange", IA_MusicSwap, player)

		inst:RemoveEventCallback("embarkboat", OnEmbark, player)
		inst:RemoveEventCallback("disembarkboat", OnDisembark, player)

		inst:RemoveEventCallback("playerentered", OnPlayerArrive)
		inst:RemoveEventCallback("OnVolcanoEruptionBegin", StartErupt, player)
		inst:RemoveEventCallback("OnVolcanoEruptionEnd", StopErupt, player)

		inst:RemoveEventCallback("QuackenEncounter", IA_StartDanger, player)
	end

	local function IA_StartSoundEmitter()
		if not _iscave then
			_isday = inst.state.isday
			inst:WatchWorldState("phase", OnPhase)
		end

		_soundemitter = TheFocalPoint.SoundEmitter
	end

	local function IA_StopSoundEmitter()
		if _soundemitter ~= nil then
			StopSailing()
			StopErupt()
			_soundemitter = nil

			inst:StopWatchingWorldState("phase", OnPhase)
		end
	end

	local function IA_OnPlayerActivated(_inst, player)
		if _activatedplayer == player then
			return
		elseif _activatedplayer ~= nil and _activatedplayer.entity:IsValid() then
			IA_StopPlayerListeners(_activatedplayer)
		end
		_activatedplayer = player
		IA_StopSoundEmitter()
		IA_StartSoundEmitter()
		IA_StartPlayerListeners(player)
	end

	local function IA_OnPlayerDeactivated(_inst, player)
		IA_StopPlayerListeners(player)
		if player == _activatedplayer then
			_activatedplayer = nil
			IA_StopSoundEmitter()
		end
	end

	local function IA_OnEnableDynamicMusic(_inst, enable)
		_isenabled = enable
		_soundemitter = TheFocalPoint.SoundEmitter

		if _isenabled ~= enable then
			if not enable and _soundemitter ~= nil then
				StopSailing()
				StopErupt()
			end
			_isenabled = enable
		end
	end

	inst:ListenForEvent("playeractivated", IA_OnPlayerActivated)
	inst:ListenForEvent("playerdeactivated", IA_OnPlayerDeactivated)
	inst:ListenForEvent("enabledynamicmusic", IA_OnEnableDynamicMusic)
end)
