local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function fn(inst)
    if TheWorld.ismastersim then
        local _COMBAT_MUSTONEOF_TAGS_AGGRESSIVE = UpvalueHacker.GetUpvalue(inst.BecomeAggressive, "AggressiveRetarget", "COMBAT_MUSTONEOF_TAGS_AGGRESSIVE")
        table.insert(_COMBAT_MUSTONEOF_TAGS_AGGRESSIVE, "abigail_target")
    end
end

IAENV.AddPrefabPostInit("abigail", fn)
