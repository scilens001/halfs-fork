local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local MonkeyKing_endings = { "", "e", "h", }
local MonkeyKing_punc = {".", "?", "!"}

local function monkeykingstart()
    local str = "O"
    local l = math.random(2, 4)
    for i = 2, l do
        str = str..(math.random() > 0.3 and "o" or "a")
    end
    return str
end

local function monkeykingspace()
    local c = math.random()
    local str =
        (c <= .1 and "! ") or
        (c <= .2 and ". ") or
        (c <= .3 and "? ") or
        (c <= .4 and ", ") or
        " "
    return str
end

local monkeykingend = function()
    return MonkeyKing_endings[math.random(1,#MonkeyKing_endings)]
end

local function monkeykingpunc()
    return MonkeyKing_punc[math.random(1,#MonkeyKing_punc)]
end

--wilburspeech is different from wonkeyspeech
function CraftMonkeyKingSpeech()
    local length = math.random(6)
    local str = ""
    for i = 1, length do
        str = str..monkeykingstart()..monkeykingend()
        if i ~= length then
            str = str..monkeykingspace()
        end
    end
    return str..monkeykingpunc()
end

local debug = debug
local giveup_index = 5

local _getcharacterstring = UpvalueHacker.GetUpvalue(GetString, "getcharacterstring")
local function __getcharacterstring(tab, item, modifier, ...)
    local ismonkey
    local stack_index = 2
    local local_index = 1
    while true do
        local name, value = debug.getlocal(stack_index, local_index)
        if name then 
            if name == "inst" then
                ismonkey = value == "wilbur" or (type(value) == "table" and value.HasTag and value:HasTag("monkeyking"))
                break
            end
            local_index = local_index + 1
        else
            local info = debug.getinfo(stack_index, "f")
            stack_index = stack_index + 1 -- 2 should be fine but increase this incase a mod changes it
            if stack_index > giveup_index or info.func == GetActionString then break end --if were past the giveup stack level or the function we just checked was GetActionString (we dont care about that one) then give up
        end
    end
	return _getcharacterstring(tab, item, modifier, ...) or (ismonkey and CraftMonkeyKingSpeech()) or nil
end

UpvalueHacker.SetUpvalue(GetString, __getcharacterstring, "getcharacterstring")
